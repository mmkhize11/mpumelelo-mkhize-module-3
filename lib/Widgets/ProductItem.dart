import 'dart:convert';

class ProductItem {
  // @required this.name,
  //     @required this.imageUrl,
  //     @required this.slug,
  //     @required this.price,

  late String name;
  late String imageUrl;
  late String slug;
  late int quantityAvailable;
  late int price;
  ProductItem({
    required this.name,
    required this.imageUrl,
    required this.quantityAvailable,
    required this.price,
  });

  ProductItem copyWith({
    String? name,
    String? imageUrl,
    int? quantityAvailable,
    int? price,
  }) {
    return ProductItem(
      name: name ?? this.name,
      imageUrl: imageUrl ?? this.imageUrl,
      quantityAvailable: quantityAvailable ?? this.quantityAvailable,
      price: price ?? this.price,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'imageUrl': imageUrl,
      'quantityAvailable': quantityAvailable,
      'price': price,
    };
  }

  factory ProductItem.fromMap(Map<String, dynamic> map) {
    return ProductItem(
      name: map['name'],
      imageUrl: map['imageUrl'],
      quantityAvailable: map['quantityAvailable']?.toInt(),
      price: map['price']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductItem.fromJson(String source) =>
      ProductItem.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProductItem(name: $name, imageUrl: $imageUrl, quantityAvailable: $quantityAvailable, price: $price)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductItem &&
        other.name == name &&
        other.imageUrl == imageUrl &&
        other.quantityAvailable == quantityAvailable &&
        other.price == price;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        imageUrl.hashCode ^
        quantityAvailable.hashCode ^
        price.hashCode;
  }
}
