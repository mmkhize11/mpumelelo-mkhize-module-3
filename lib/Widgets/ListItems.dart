import 'package:flutter/material.dart';
import 'package:module3/Widgets/ProductItem.dart';
import 'package:module3/Widgets/ProductsTile.dart';

class ListItems extends StatefulWidget {
  List<ProductItem>? products;
  ListItems({Key? key, required this.products}) : super(key: key);

  @override
  State<ListItems> createState() => _ListItemsState();
}

class _ListItemsState extends State<ListItems> {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
//    physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.all(1.0),
      childAspectRatio: 8.0 / 12.0,
      children: List<Widget>.generate(widget.products!.length, (index) {
        return GridTile(
            child: ProductTile(
          passedItem: widget.products![index],
        ));
      }),
    );
  }
}
