import 'package:flutter/material.dart';
import 'package:module3/Widgets/ListItems.dart';
import 'package:module3/Widgets/ProductItem.dart';

class ShoesPage extends StatefulWidget {
  const ShoesPage({Key? key}) : super(key: key);

  @override
  State<ShoesPage> createState() => _ShoesPageState();
}

class _ShoesPageState extends State<ShoesPage> {
  List<ProductItem> ShoesProducts = [
    ProductItem(
        name: "Nike Air Max 2021 ",
        imageUrl:
            "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/e5af7319-a671-4187-a10a-020e09e7b3db/air-max-2021-shoes-w7ffPL.png",
        quantityAvailable: 5,
        price: 3000),
    ProductItem(
        name: "air-max-terrascape-90-shoes",
        imageUrl:
            "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/35ed748e-8b28-4421-87a8-6f2cfdc53b73/air-max-terrascape-90-shoes-1ZpGGk.png",
        quantityAvailable: 5,
        price: 2800),
    ProductItem(
        name: "Nike Air Force 1 '07 LV8 ",
        imageUrl:
            "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/ac4c446a-0156-4ebc-863d-5820c5bf20cb/air-force-1-07-lv8-shoes-x7SbPm.png",
        quantityAvailable: 5,
        price: 2100),
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListItems(
        products: ShoesProducts,
      ),
    );
  }
}
