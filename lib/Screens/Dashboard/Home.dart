import 'package:flutter/material.dart';
import 'package:module3/Screens/Cart/Cart.dart';
import 'package:module3/Screens/Dashboard/HomeBody.dart';
import 'package:module3/Screens/Favourites/Favourites.dart';
import 'package:module3/Screens/Profile/Profile.dart';
import 'package:module3/Screens/Settings/Settings.dart';
import 'package:settings_ui/settings_ui.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  late List<Widget> _pages;
  String appBarTitle = "Home";
  @override
  void initState() {
    super.initState();

    _pages = [HomeBody(), Favourites(), Cart()];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(appBarTitle)),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: _pages[_selectedIndex],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            UserAccountsDrawerHeader(
              accountName: Text("User"),
              accountEmail: Text("User@gmail.com"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"),
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                    "http://api.androidhive.info/images/nav-menu-header-bg.jpg",
                  ),
                  fit: BoxFit.fill,
                ),
              ),
              otherAccountsPictures: [
                // CircleAvatar(
                //   backgroundColor: Colors.white,
                //   backgroundImage: NetworkImage(
                //       "https://randomuser.me/api/portraits/women/74.jpg"),
                // ),
                // CircleAvatar(
                //   backgroundColor: Colors.white,
                //   backgroundImage: NetworkImage(
                //       "https://randomuser.me/api/portraits/men/47.jpg"),
                // ),
              ],
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: const Text(' My Profile '),
              onTap: () {
                // Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Profile()));
              },
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: const Text('LogOut'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  // size: 150,
                ),
                label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.favorite,
                  // size: 150,
                ),
                label: 'Favourite'),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.shopping_bag_rounded,
                  // size: 150,
                ),
                label: 'Cart')
          ]

          // const <BottomNavigationBarItem>[

          // currentIndex: _selectedIndex, //New
          // onTap: _onItemTapped,         //New
          ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // GalleryScreen();
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Settings()));
        },
        child: const Icon(Icons.settings),
        backgroundColor: Colors.blue,
      ),
    );
  }

  void _onItemTapped(int index) {
    switch (index) {
      case 0:
        {
          setState(() {
            appBarTitle = "Home";
          });
        }
        break;

      case 1:
        {
          setState(() {
            appBarTitle = "Favourites";
          });
        }
        break;

      case 2:
        {
          setState(() {
            appBarTitle = "Cart";
          });
        }
    }
    setState(() {
      // if(){

      // }
      _selectedIndex = index;
    });
  }
}
