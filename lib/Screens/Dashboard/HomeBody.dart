import 'package:flutter/material.dart';
import 'package:module3/Screens/ShirtsPage/ShirtsPage.dart';
import 'package:module3/Screens/ShoesPage/ShoesPage.dart';
import 'package:module3/Screens/BottomsPage/BottomsPage.dart';
// import 'package:module3/Screens/TopsPage/TopsPage.dart';
import 'package:module3/Widgets/SearchBar.dart';

class HomeBody extends StatefulWidget {
  const HomeBody({Key? key}) : super(key: key);

  @override
  State<HomeBody> createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> with TickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(
      initialIndex: 0,
      length: 2,
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child:
          // SingleChildScrollView(
          //   child:
          Column(
        children: [
          SearchBar(),
          SizedBox(
            height: 10,
            child: Container(
              color: Color(0xFFf5f6f7),
            ),
          ),
          PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: TabBar(
              controller: tabController,
              labelColor: Colors.black,
              tabs: [
                Tab(
                  text: 'Shoes',
                ),
                Tab(
                  text: 'Shirts',
                ),
                Tab(
                  text: 'Tops',
                )
              ], // list of tabs
            ),
          ),
          SizedBox(
            height: 10,
            child: Container(
              color: Color(0xFFf5f6f7),
            ),
          ),
          Expanded(
              child: TabBarView(
            controller: tabController,
            children: [
              Container(
                color: Colors.white24,
                child: ShoesPage(),
                // child: CategoryPage(slug: 'categories/'),
              ),
              Container(
                color: Colors.white24,
                child: ShirtsPage(),
                // child: BrandHomePage(slug: 'brands/?limit=20&page=1'),
              ),
              Container(
                color: Colors.white24,
                child: BottomsPage(),
                // child: ShopHomePage(
                //   slug: 'custom/shops/?page=1&limit=15',
                // ),
              ) //0 class name
            ],
          )),
        ],
      ),
      // ),
    );
  }
}
