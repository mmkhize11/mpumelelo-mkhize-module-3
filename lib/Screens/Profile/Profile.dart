import 'package:flutter/material.dart';
import 'package:module3/Screens//Profile/ProfileBody.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Profile')),
      body: ProfileBody(),
    );
  }
}
