import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:module3/Common/Common.dart';
import 'package:module3/Screens/Dashboard/Home.dart';
// import 'package:module3/Home/Home.dart';
import 'package:module3/Widgets/default_button.dart';
import 'package:module3/Widgets/form_error.dart';
import 'package:module3/Widgets/no_account_text.dart';

class LoginBody extends StatefulWidget {
  const LoginBody({Key? key}) : super(key: key);

  @override
  State<LoginBody> createState() => _LoginBodyState();
}

class _LoginBodyState extends State<LoginBody> {
  late String email;

  bool remember = false;
  late Timer _timer;
  late String password;
  final List<String> errors = [];
  final _formKey = GlobalKey<FormBuilderState>();

  bool _isObscure = true;
  // late UserModel currentUser;
  bool ChangesHaveBeenMade = false;

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error!);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  var loading = Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Platform.isIOS == true
          ? CupertinoActivityIndicator(

              //   valueColor: AlwaysStoppedAnimation<Color>(
              //   LIGHTBLUEHTML,
              //   // Theme.of(context).primaryColor, /
              //   /// Red
              // ),
              )
          : CircularProgressIndicator(
              // value: downloadProgress.progress,
              valueColor: AlwaysStoppedAnimation<Color>(
                Colors.blue,
                // Theme.of(context).primaryColor, /
                /// Red
              ),
            ),
      Text(" Authenticating ... Please wait")
    ],
  );

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: FormBuilder(
          key: _formKey,
          child: Column(
            children: [
              buildEmailFormField(),
              SizedBox(height: 30),
              buildPasswordFormField(),
              SizedBox(height: 30),
              Row(
                children: [
                  Checkbox(
                    value: remember,
                    activeColor: Colors.blue,
                    onChanged: (value) {
                      setState(() {
                        remember = value!;
                      });
                    },
                  ),
                  Text("Remember me"),
                  Spacer(),
                  GestureDetector(
                    onTap: () => {
                      // Navigator.push(context,
                      //     MaterialPageRoute(builder: (context) => Home()))
                    },
                    child: Text(
                      "Forgot Password",
                      style: TextStyle(
                          // decoration: TextDecoration.underline,
                          color: Colors.blue),
                    ),
                  )
                ],
              ),
              FormError(errors: errors),
              SizedBox(height: 30),
              DefaultButton(
                isLoading: false,
                text: "Login",
                press: () {
                  if (_formKey.currentState!.validate()) {
                    _formKey.currentState!.save();
                    final data =
                        Map<String, dynamic>.from(_formKey.currentState!.value);

                    // GestureDetector(onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Home()));
                    // Home
                    // });
                  }
                },
              ),
              SizedBox(
                height: 30,
              ),
              // SizedBox(height: 20),
              NoAccountText()
            ],
          ),
        ),
      ),
    );
    ;
  }

  FormBuilderTextField buildEmailFormField() {
    return FormBuilderTextField(
      name: "Email",
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => email = newValue!,
      onChanged: (value) {
        if (value!.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Email",
        hintText: "Enter your email",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.email, color: Colors.blue),
      ),
    );
  }

  FormBuilderTextField buildPasswordFormField() {
    return FormBuilderTextField(
      name: "Password",
      obscureText: _isObscure,
      onSaved: (newValue) => password = newValue!,
      onChanged: (value) {
        if (value!.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Password",
          hintText: "Enter your password",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: IconButton(
              icon: Icon(_isObscure ? Icons.visibility_off : Icons.visibility,
                  color: _isObscure ? Colors.grey : Colors.blue),
              onPressed: () {
                setState(() {
                  _isObscure = !_isObscure;
                });
              })),
      // suffixIcon: Icon(Icons.password, color: LIGHTBLUEHTML)
      // prefixIcon: Icon(Icons.remove_red_eye, color: LIGHTBLUEHTML)),
    );
  }
}
