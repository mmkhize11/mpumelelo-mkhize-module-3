final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please Enter your email";
const String kSubjectNullError = "Please Enter your Subject";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNamelNullError = "Please Enter your firstname";
const String klastNamelNullError = "Please Enter your lastname";
const String kPhoneNumberNullError = "Please Enter your phone number";
const String kTextNullError = "Please Enter your Message";
const String kAddressNullError = "Please Enter your address";
const String kMissingRequiredFieldError =
    "Please fill in the Required Fields  ";
